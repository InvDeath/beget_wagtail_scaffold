##! make

# beget_wagtail_scaffold
PROJECT_NAME = begetwagtailscaffold

init:
	docker-compose run --rm web python manage.py collectstatic --noinput
	docker-compose run --rm web python manage.py migrate
	sudo touch deploy/.vault_pass
	cp ~/.ssh/id_rsa deploy/.id_rsa
	cp ~/.ssh/id_rsa.pub deploy/.id_rsa.pub
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data -v $(shell pwd)/app/static:/static miguelalvarezi/nodejs-bower-gulp sh -c 'npm install && bower i --allow-root --config.interactive=false && gulp build'
	$(MAKE) fix_perms

frontend_shell:
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data miguelalvarezi/nodejs-bower-gulp bash

frontend_watch:
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data -v $(shell pwd)/app/static:/static miguelalvarezi/nodejs-bower-gulp sh -c 'npm install && bower i --allow-root --config.interactive=false && gulp'

shell:
	docker-compose run --rm web bash

clear_docker:
	docker stop $(shell docker ps -a -q)
	docker rm $(shell docker ps -a -q)
	docker rmi $(shell docker ps -a -q)

fix_perms:
	sudo chmod -R 777 .
	sudo chmod -R 666 deploy/hosts deploy/.vault_pass

run:
	docker-compose up -d
	docker inspect --format 'http://{{ .NetworkSettings.IPAddress }}:8000' $(PROJECT_NAME)_web_1

manage:
	docker-compose run --rm web python manage.py $(COMMAND)

load_data:
	$(MAKE) manage COMMAND='loaddata app/app/fixtures/dump.json'
	mkdir -p data/public/media
	cp -r app/app/fixtures/media data/public

.PHONY: deploy
deploy:
	tar -cf ./deploy/.deploy.tar app
	docker build -t $(PROJECT_NAME)/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data $(PROJECT_NAME)/deploy ansible-playbook server.yml --tags="deploy"
	rm ./deploy/.deploy.tar

backup:
	docker run --rm -ti -v $(shell pwd)/deploy:/data $(PROJECT_NAME)/deploy ansible-playbook deploy/server.yml --tags="backup"

rollback:
	docker run --rm -ti -v $(shell pwd)/deploy:/data $(PROJECT_NAME)/deploy ansible-playbook deploy/server.yml --tags="rollback"

dump:
	docker run --rm -ti -v $(shell pwd)/deploy:/data $(PROJECT_NAME)/deploy ansible-playbook deploy/server.yml --tags="dump"
